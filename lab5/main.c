#include <stdio.h>
#include <mpi.h>
#include <pthread.h>
#include <stdlib.h>
#include <math.h>

#define  COUNT 300

double globalRes = 0;
int iterCounter = 0;
int theFirstInThisProcess;
int theLastPlusOneInThisProcess;
pthread_mutex_t mutex;
int doSendRecv = 1;
int devided;
int countOfFinishedTask = 0;

void task(int number) {
  int cnt = abs(iterCounter + number * number * number + 1);
  for (int i = 0; i < cnt; ++i) globalRes += sin(i);
  countOfFinishedTask++;
  //printf("%d complete! It computed %d times\n",number,cnt);
}

void *work(void *a) {
  int cnt = 0;

  while (1) {
    while (theFirstInThisProcess < theLastPlusOneInThisProcess) {

      task(theFirstInThisProcess);

      pthread_mutex_lock(&mutex);
      ++theFirstInThisProcess;
      pthread_mutex_unlock(&mutex);
      ++cnt;
    }
    if (!doSendRecv) {
      break;
    }

    //Просим добавки
    MPI_Send(&cnt, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);
    int realI;
    //принимаем номер процесса у которого нужно взять задание или код остановки -2
    MPI_Recv(&realI, 1, MPI_INT, 0, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    if (realI != -2) {
      int mes = -1;//просим у процесса realI добавки
      MPI_Send(&mes, 1, MPI_INT, realI, 1, MPI_COMM_WORLD);
      int newLine[2];//делаем задания с newLine[0] - newLine[1]
      MPI_Recv(newLine, 2, MPI_INT, realI, 3, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      pthread_mutex_lock(&mutex);//

      theFirstInThisProcess = newLine[0];
      theLastPlusOneInThisProcess = newLine[1];

      pthread_mutex_unlock(&mutex);
    } else {
      break;
    }
  }
}

int getTheFirstInThisProcessEq(int I, int cntPr, int N) {//распределяем поровну
  if (I < N % cntPr) {
    return (N / cntPr + 1) * I;
  }
  return N - (N / cntPr) * (cntPr - I);
}

int getCntStrInThisProcessEq(int I, int cntPr, int N) {
  if (I < N % cntPr) {
    return N / cntPr + 1;
  }
  return N / cntPr;
}

int getTheFirstInThisProcessOne(int I, int cntPr, int N) {//все первому, остальные получают 0
  if (!I) return 0;
  return N;
}

int getCntStrInThisProcessOne(int I, int cntPr, int N) {
  if (!I) return N;
  return 0;
}

int getTheFirstInThisProcessMd(int I, int cntPr, int N) {//рапределяем поровну, но сложности рандомно
  int d = 1;
  int step = N / (cntPr * ((cntPr - 1) * d) / 2);
  return step * (I) * (d * (I - 1)) / 2;
}

int getCntStrInThisProcessMd(int I, int cntPr, int N) {
  int d = 1;
  int step = N / (cntPr * ((cntPr - 1) * d) / 2);
  if (I != cntPr - 1) return I * step;
  return N - step * (cntPr - 1) * ((cntPr - 2) * d) / 2;
}

typedef struct table {
  int *addr;
  int cntProc;
} table;

void initT(table *T, int cntPr) {

  T->addr = (int *) malloc(sizeof(int *) * cntPr);
  for (int i = 0; i < cntPr; ++i) {
    T->addr[i] = rand() % cntPr;
  }
  T->cntProc = cntPr;
}

/*Фактически table - это таблица того, как давно не отвечал процесс. Как только процесс дал сигнал 0-му о окончании работы, тот
Вызывает эту функцию. Она ставит 0 в поле запрашивающего процесса, находит процесс с  максимальным индексом. Индекс становится равным нулю
(т.к у него только что забрали половину работы, он не интересен в обозримом будущем), а его номер отысылается запросившему,
который в свою очереь уже запрашивает дополнительные данные.*/

int getOverloadProcess(table *T, int a) {//определение
  int max = -1, maxi;
  int g = T->cntProc;
  T->addr[a] = -1;
  for (int i = 0; i < g; ++i) {
    if (T->addr[i] > max) {
      maxi = i;
      max = T->addr[i];
    }
    (T->addr[i])++;
  }
  T->addr[maxi] = 0;
  return maxi;
}

void destroy(table *T) { free(T->addr); }

int main(int argc, char *argv[]) {

  devided = atoi(argv[1]);
  int rank, cntPr;
  int provided;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &cntPr);
  if (!I && provided != MPI_THREAD_MULTIPLE) {
    perror("ERROR PROVIDED");
    MPI_Finalize();
  }

  //sleep(1);
  if (pthread_mutex_init(&mutex, NULL) != 0) {
    perror("ERROR mutex_init");
    MPI_Finalize();
    return 0;
  }

  pthread_attr_t attr;
  pthread_t worker;
  double finishTime;


  //До этого момента выполнялась Инициализация.
  finishTime = MPI_Wtime();
  while (iterCounter < 3) {
    countOfFinishedTask = 0;
    globalRes = 0;
    //Начало новой глобальной итерации
    double t1, t2;
    t1 = MPI_Wtime();
    int N;
    int cntWas = 0;
    if (rank == 0) {
      N = COUNT;
      //Установка числа заданий
    }

    MPI_Bcast(&N, 1, MPI_INT, 0, MPI_COMM_WORLD);

    //Определение заданий, которые будет выполнять каждый процесс изначально
    if (devided == 0) {
      theFirstInThisProcess = getTheFirstInThisProcessEq(rank, cntPr, N);
      theLastPlusOneInThisProcess = theFirstInThisProcess + getCntStrInThisProcessEq(rank, cntPr, N);
    }
    if (devided == 1) {
      theFirstInThisProcess = getTheFirstInThisProcessOne(rank, cntPr, N);
      theLastPlusOneInThisProcess = theFirstInThisProcess + getCntStrInThisProcessOne(rank, cntPr, N);
    }
    if (devided == 2) {
      theFirstInThisProcess = getTheFirstInThisProcessMd(rank, cntPr, N);
      theLastPlusOneInThisProcess = theFirstInThisProcess + getCntStrInThisProcessMd(rank, cntPr, N);
    }

    table t;
    initT(&t, cntPr);

    //Инициализируем потоки
    if (pthread_attr_init(&attr) != 0) {
      perror("ERROR ATTR_INIT");
      MPI_Finalize();
      return 0;
    }

    if (pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE) != 0) {
      perror("ERROR attr setdetachstate");
      MPI_Finalize();
      return 0;
    }

    //Создаём поток, который будет заниматься основной работой.
    //Фактически здесь происходит разветвление , один поток продолжит исполнение кода отсбда же, а второй -  функции work()
    if (pthread_create(&worker, &attr, work, NULL) != 0) {
      perror("ERROR thread_create(&worker,&attr...");
      if (pthread_attr_destroy(&attr) != 0) {
        perror("ERROR attr destroy");
      }
      MPI_Finalize();
      return 0;
    }
    int cntEnd = 0;
    //Место в коде, которое занимается распределение работы. Если doSendRecv ==0 то программа превращается в последовательную
    if (doSendRecv) {

      if (rank == 0) {
        //Нулевой процесс помимо превычного формирования заданий отвечает и за контроль выполнением заданий
        while (cntEnd < cntPr) {
          int mes = 1;
          MPI_Status status;

          MPI_Recv(&mes, 1, MPI_INT, MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, &status);
          if (mes >= 0) {
            //Увеличиваем количество выполненных заданий.
            //Если задания кончились - отдаём команду на финиширование всем процессам
            cntWas += mes;

            if (cntWas < N) {
              //Возращает процесс, которому нужна помощь и посылает запросившему информацию к кому нужно обратится для доп. заданий
              int rec = getOverloadProcess(&t, status.MPI_SOURCE);

              MPI_Send(&rec, 1, MPI_INT, status.MPI_SOURCE, 2, MPI_COMM_WORLD);
            } else {
              //Заданий больше нет. Команда остановки запросившему процессу.
              int rec = -2;
              ++cntEnd;
              MPI_Send(&rec, 1, MPI_INT, status.MPI_SOURCE, 2, MPI_COMM_WORLD);
            }
          }

          if (mes == -1) {
            //Формирование и отправка задания (нулевой тоже умеет это делать)
            pthread_mutex_lock(&mutex);//
            int forAnother[2];
            forAnother[1] = theLastPlusOneInThisProcess;
            theLastPlusOneInThisProcess = (theFirstInThisProcess + theLastPlusOneInThisProcess) / 2;
            forAnother[0] = theLastPlusOneInThisProcess;
            pthread_mutex_unlock(&mutex);
            MPI_Send(forAnother, 2, MPI_INT, status.MPI_SOURCE, 3, MPI_COMM_WORLD);
            //printf("%d helps %d with %d-%d\n",status.MPI_SOURCE,I,forAnother[0],forAnother[1]-1);
          }

        }

        //Финал. Отправка всем прочим процессам команды остановки
        int mes = -3;
        for (int i = 1; i < cntPr; ++i) {
          MPI_Send(&mes, 1, MPI_INT, i, 1, MPI_COMM_WORLD);
        }
      } else {//не нулевой процесс
        int mes = 1;
        while (1) {
          MPI_Status status;
          //Процесс ждёт сообщения от кого-нибудь, после чего приступает к формированию задания и отправляет новые границы
          //Если сообщение "-3" - признак остановки
          MPI_Recv(&mes, 1, MPI_INT, MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, &status);
          if (mes == -3) {
            break;
          }

          pthread_mutex_lock(&mutex);
          int forAnother[2];
          //Отдаём половину собственных заданий.
          //Поскольку мы блокируем поток, изменение глобальной переменной безопасно
          forAnother[1] = theLastPlusOneInThisProcess;
          theLastPlusOneInThisProcess = (theFirstInThisProcess + theLastPlusOneInThisProcess) / 2;
          forAnother[0] = theLastPlusOneInThisProcess;
          pthread_mutex_unlock(&mutex);
          MPI_Send(forAnother, 2, MPI_INT, status.MPI_SOURCE, 3, MPI_COMM_WORLD);
          // printf("%d helps %d with %d-%d\n",status.MPI_SOURCE,I,forAnother[0],forAnother[1]-1);
        }
      }
    }
    void *a;

    //Точка синхронизации. Ждём пока все потоки доберуться до сюда.
    if (pthread_join(worker, &a) != 0) {
      perror("ERROR pthread_join(worker...");
      MPI_Finalize();
      return 0;
    }
    destroy(&t);
    ++iterCounter;
    ++devided;
    //Замерка времени на данной итерации и вывод дополнительных данных
    t2 = MPI_Wtime() - t1;
    printf("\nThis is %d procces, Iteration %d\n, I've made %d tasks", I, iterCounter, countOfFinishedTask);

    printf("\n--------- RESULT --------\n");
    printf("Time: %f  sec.  Result: %f\n", t2, globalRes);
  }

  finishTime = MPI_Wtime() - finishTime;

  if (pthread_mutex_destroy(&mutex) != 0) {
    perror("ERROR mutex_destroy");
    //MPI_Finalize();
    //return 0;
  }
  if (pthread_attr_destroy(&attr) != 0) {
    perror("ERROR attr destroy");
    //MPI_Finalize();
    //return 0;

    MPI_Barrier(MPI_COMM_WORLD);
  }
  //printf("%d: theFirstInThisProcess=%d\n",I,theFirstInThisProcess);

  /*t1 = t2 - t1;
  MPI_Allreduce(&t1, &t2, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);*/

  if (rank == 0) {
    printf("\n--------- Summary time taken --------\n");
    printf("Time: %f  sec.  %f\n", finishTime, globalRes);
  }
  MPI_Finalize();
  return 0;
}


