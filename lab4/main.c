#include <mpi.h>
#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include <string.h>

#include "myalloc.h"

#define A (1e5)
#define EPS (1e-8)

#define Index(x, y, z) (nodes_per_lvl * (z) + (y) * nx + x)

void swap(double **a, double **b) {
  double *pSwap = *a;
  *a = *b;
  *b = pSwap;
}

double max(double a, double b) {
  if (a > b)
    return a;
  else
    return b;
}

double phi(double x, double y, double z) {
  return x * x + y * y + z * z;
}

double rho(double x, double y, double z) {
  return 6.0 - A * phi(x, y, z);
}

int main(int argc, char **argv) {
  int nx, ny, nz;
  nx = 100;//atoi(argv[1]);
  ny = 200;//atoi(argv[2]);
  nz = 800;//atoi(argv[3]);

  int print_rank = 0;
  if (argc > 4) {
    print_rank = atoi(argv[4]);
  }

  int wsize;
  int rank = 0;

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &wsize);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  if (2 * wsize > nz) {
    if (rank == 0)
      printf("Too small nz");
    MPI_Finalize();
    return -1;
  }

  MPI_Request send_up_req = 0;
  MPI_Request rec_up_req = 0;
  MPI_Request send_low_req = 0;
  MPI_Request rec_low_req = 0;

  double dx = 2.0;
  double dy = 2.0;
  double dz = 2.0;

  double x0 = -1.0;
  double y0 = -1.0;
  double z0 = -1.0;

  double hx, hy, hz;
  /* square */
  double hx_sq, hy_sq, hz_sq;
  double factor = 1.0;
  /* number of levels in this proc */
  int lvl_cnt;
  /* number of nodes at one level ( = nx * ny) */
  int nodes_per_lvl;
  int nodes_per_proc;
  int node_cnt;

  /* first level of current process */
  int lvl;

  double *data;
  double *tmp_data;
  double *top_buff;
  double *bot_buff;

  /* number of nodes on ith proc */
  int *nodes;
  /* number of levels on ith proc */
  int *lvls;

  //Init the data

  hx = dx / (nx - 1);
  hy = dy / (ny - 1);
  hz = dz / (nz - 1);

  hx_sq = pow(hx, 2.0);
  hy_sq = pow(hy, 2.0);
  hz_sq = pow(hz, 2.0);

  factor = 1.0 / (2.0 / pow(hx, 2.0) + 2.0 / pow(hy, 2.0) + 2.0 / pow(hz, 2.0) + A);

  node_cnt = nx * ny * nz;
  nodes_per_lvl = nx * ny;

  nodes = (int *) allocate(wsize * sizeof(int));
  if (nodes == NULL) {
    char str[5];
    sprintf(str, "%d", __LINE__);
    perror(str);
    freeall();
  }

  lvls = (int *) allocate(wsize * sizeof(int));
  if (lvls == NULL) {
    char str[5];
    sprintf(str, "%d", __LINE__);
    perror(str);
    freeall();
  }

  for (int i = 0; i < wsize; ++i) {
    lvls[i] = (nz / wsize) + ((i + 1) <= nz % wsize);
    nodes[i] = lvls[i] * nodes_per_lvl;
  }
  lvl_cnt = lvls[rank];

  int sum = 0;
  for (int i = 0; i < wsize; ++i) {
    if (i == rank)
      lvl = sum;
    sum += lvls[i];
  }

  data = (double *) allocate(nodes[rank] * sizeof(double));
  if (data == NULL) {
    char str[5];
    sprintf(str, "%d", __LINE__);
    perror(str);
    freeall();
  }

  tmp_data = (double *) allocate(nodes[rank] * sizeof(double));
  if (tmp_data == NULL) {
    char str[5];
    sprintf(str, "%d", __LINE__);
    perror(str);
    freeall();
  }

  if (rank != wsize - 1) {
    top_buff = (double *) allocate(nx * ny * sizeof(double));
    if (top_buff == NULL) {
      char str[5];
      sprintf(str, "%d", __LINE__);
      perror(str);
      freeall();
    }

    for (int i = 0; i < nodes_per_lvl; ++i) {
      top_buff[i] = 0.0;
    }
  } else {
    top_buff = NULL;
  }

  if (rank != 0) {
    bot_buff = (double *) allocate(nx * ny * sizeof(double));
    if (bot_buff == NULL) {
      char str[5];
      sprintf(str, "%d", __LINE__);
      perror(str);
      freeall();
    }

    for (int i = 0; i < nodes_per_lvl; ++i) {
      bot_buff[i] = 0.0;
    }
  } else {
    bot_buff = NULL;
  }

  if (rank == 0) {
    for (int i = 0; i < nx; ++i)
      for (int j = 0; j < ny; ++j) {
        data[Index(i, j, 0)] = phi(x0 + hx * (i), y0 + hy * (j), z0 + hz * (0));
        tmp_data[Index(i, j, 0)] = data[Index(i, j, 0)];
      }
  }

  if (rank == wsize - 1) {
    for (int i = 0; i < nx; ++i)
      for (int j = 0; j < ny; ++j) {
        data[Index(i, j, lvl_cnt - 1)] = phi(x0 + hx * (i), y0 + hy * (j), z0 + hz * (lvl + lvl_cnt - 1));
        tmp_data[Index(i, j, lvl_cnt - 1)] = data[Index(i, j, lvl_cnt - 1)];

      }
  }

  for (int k = 0; k < lvls[rank]; ++k) {
    for (int i = 0; i < nx; ++i) {
      data[Index(i, 0, k)] = phi(x0 + hx * (i), y0, z0 + hz * (lvl + k));
      data[Index(i, ny - 1, k)] = phi(x0 + hx * (i), y0 + hy * (ny - 1), z0 + hz * (lvl + k));

      tmp_data[Index(i, 0, k)] = data[Index(i, 0, k)];
      tmp_data[Index(i, ny - 1, k)] = data[Index(i, ny - 1, k)];

    }

    for (int j = 0; j < ny; ++j) {
      data[Index(0, j, k)] = phi(x0 + hx * (0), y0 + hy * (j), z0 + hz * (lvl + k));
      data[Index(nx - 1, j, k)] = phi(x0 + hx * (nx - 1), y0 + hy * (j), z0 + hz * (lvl + k));

      tmp_data[Index(0, j, k)] = data[Index(0, j, k)];
      tmp_data[Index(nx - 1, j, k)] = data[Index(nx - 1, j, k)];

    }
  }

  MPI_Barrier(MPI_COMM_WORLD);

  //End of init

  int iter_cnt = 0;
  int total_iter = 0;

  double time = MPI_Wtime();

  while (true) {
    ++total_iter;
    int flag = 1;

    /* recount border values */

    if (rank != wsize - 1) {
      for (int i = 1; i < nx - 1; ++i)
        for (int j = 1; j < ny - 1; ++j) {
          double xsum = data[Index(i - 1, j, lvl_cnt - 1)] + data[Index(i + 1, j, lvl_cnt - 1)];
          double ysum = data[Index(i, j + 1, lvl_cnt - 1)] + data[Index(i, j - 1, lvl_cnt - 1)];
          double zsum = top_buff[Index(i, j, 0)] + data[Index(i, j, lvl_cnt - 2)];

          tmp_data[Index(i, j, lvl_cnt - 1)] = factor * (xsum / hx_sq + ysum / hy_sq + zsum / hz_sq
              - rho(x0 + hx * (i), y0 + hy * (j), z0 + hz * (lvl + lvl_cnt - 1)));
        }

    }

    if (rank != 0) {
      for (int i = 1; i < nx - 1; ++i)
        for (int j = 1; j < ny - 1; ++j) {
          double xsum = data[Index(i - 1, j, 0)] + data[Index(i + 1, j, 0)];
          double ysum = data[Index(i, j + 1, 0)] + data[Index(i, j - 1, 0)];
          double zsum = bot_buff[Index(i, j, 0)] + data[Index(i, j, 1)];

          tmp_data[Index(i, j, 0)] = factor
              * (xsum / hx_sq + ysum / hy_sq + zsum / hz_sq - rho(x0 + hx * (i), y0 + hy * (j), z0 + hz * (lvl)));
        }
    }



/* send data to upper process */

    if (rank != wsize - 1) {
      MPI_Isend(tmp_data + Index(0, 0, lvl_cnt - 1),
                nodes_per_lvl,
                MPI_DOUBLE,
                rank + 1,
                0,
                MPI_COMM_WORLD,
                &send_up_req);
      MPI_Irecv(top_buff, nodes_per_lvl, MPI_DOUBLE, rank + 1, 0, MPI_COMM_WORLD, &rec_up_req);
    }

/* send data to lower process */

    if (rank != 0) {
      MPI_Isend(tmp_data, nodes_per_lvl, MPI_DOUBLE, rank - 1, 0, MPI_COMM_WORLD, &send_low_req);
      MPI_Irecv(bot_buff, nodes_per_lvl, MPI_DOUBLE, rank - 1, 0, MPI_COMM_WORLD, &rec_low_req);
    }

    if (rank != wsize - 1) {
      MPI_Test(&send_up_req, &flag, MPI_STATUS_IGNORE);
      MPI_Test(&rec_up_req, &flag, MPI_STATUS_IGNORE);
    }

    if (rank != 0) {
      MPI_Test(&send_low_req, &flag, MPI_STATUS_IGNORE);
      MPI_Test(&rec_low_req, &flag, MPI_STATUS_IGNORE);
    }

    for (int k = 1; k < lvl_cnt - 1; ++k) {
      for (int i = 1; i < nx - 1; ++i) {
        for (int j = 1; j < ny - 1; ++j) {
          double xsum = data[Index(i - 1, j, k)] + data[Index(i + 1, j, k)];
          double ysum = data[Index(i, j + 1, k)] + data[Index(i, j - 1, k)];
          double zsum = data[Index(i, j, k - 1)] + data[Index(i, j, k + 1)];
          tmp_data[Index(i, j, k)] = factor
              * (xsum / hx_sq + ysum / hy_sq + zsum / hz_sq - rho(x0 + hx * (i), y0 + hy * (j), z0 + hz * (lvl + k)));
        }
      }
    }

    if (rank != wsize - 1) {
      MPI_Wait(&send_up_req, MPI_STATUS_IGNORE);
      MPI_Wait(&rec_up_req, MPI_STATUS_IGNORE);
    }

    if (rank != 0) {
      MPI_Wait(&send_low_req, MPI_STATUS_IGNORE);
      MPI_Wait(&rec_low_req, MPI_STATUS_IGNORE);
    }

    swap(&data, &tmp_data);

    double max_diff = 0;

    for (int k = 0; k < lvl_cnt; ++k)
      for (int i = 0; i < nx; ++i)
        for (int j = 0; j < ny; ++j)
          max_diff = max(max_diff, fabs(data[Index(i, j, k)] - phi(x0 + hx * (i), y0 + hy * (j), z0 + hz * (lvl + k))));

    bool check = max_diff < EPS;
    bool check_res = 0;

    MPI_Allreduce(&check, &check_res, 1, MPI_CHAR, MPI_LAND, MPI_COMM_WORLD);

    if (check_res)
      break;

  }

  time = MPI_Wtime() - time;

  if (rank == 0) {
    printf("Iterations %d\n", total_iter);
    printf("\n--------- TIME TAKEN --------\n");
    printf("%f sec.\n", time);
  }

  freeall();

  MPI_Finalize();
  return 0;
}
