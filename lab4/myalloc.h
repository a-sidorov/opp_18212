//
// Created by sasch on 06.04.2020.
//

#ifndef LAB1__MYALLOC_H_
#define LAB1__MYALLOC_H_

#include <stdlib.h>
#include <limits.h>
#include <stdio.h>
#include <memory.h>

void *allocate(size_t size);

void freeall(void);

#endif //LAB1__MYALLOC_H_
