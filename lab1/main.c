#include <stdio.h>
#include <math.h>
#include <string.h>

#include <mpi.h>

#include "myalloc.h"

#define offset(x, y, matrix_size) ((x)*(matrix_size) + (y))
#define TAU 1E-5
#define EPS 1E-9

static void MatrVecMUL(double *Res,
                       const double *A,
                       const double *x,
                       const size_t matrix_size,
                       const size_t rowsPerChunk) {

  for (size_t i = 0; i < rowsPerChunk; ++i) {
    Res[i] = 0.0;
    for (size_t j = 0; j < matrix_size; ++j) {
      Res[i] += A[offset(i, j, matrix_size)] * x[j];
    }
  }
}

static void NumVecMul(double *a, const double num, const size_t matrix_size) {
  for (size_t i = 0; i < matrix_size; ++i) {
    a[i] *= num;
  }
}

static void VecSub(double *a, const double *b, const size_t matrix_size) {
  for (size_t j = 0; j < matrix_size; ++j) {
    a[j] -= b[j];
  }
}

static double VecNorm(const double *x, const size_t matrix_size) {
  double sum = 0;
  for (size_t i = 0; i < matrix_size; ++i) {
    sum += x[i] * x[i];
  }
  return sqrt(sum);
}

int main(int argc, char **argv) {

  MPI_Init(&argc, &argv);

  int rank, size;

  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  const int matrix_size = 2500;
  double *A = NULL;

  if (rank == 0) {
    A = allocate(matrix_size * matrix_size * sizeof(double));
    if (A == NULL) {
      perror("Can not allocate memory 61 str");
      return 1;
    }

    for (size_t i = 0; i < matrix_size; ++i) {
      for (size_t j = 0; j < matrix_size; ++j) {
        if (i == j) {
          A[offset(i, j, matrix_size)] = 2.0;
        } else {
          A[offset(i, j, matrix_size)] = 1.0;
        }
      }
    }

  }

  double *b = allocate(matrix_size * sizeof(double));
  if (b == NULL) {
    perror("Can not allocate memory 79 str");
    freeall();
    return 1;
  }

  const double bvalue = matrix_size + 1.0;

  for (int i = 0; i < matrix_size; ++i) {
    b[i] = bvalue;
  }

  double *x0 = allocate(matrix_size * sizeof(double));

  if (x0 == NULL) {
    perror("Can not allocate memory 95 str");
    freeall();
    return 1;
  }

  double *tmp = allocate(matrix_size * sizeof(double));

  if (tmp == NULL) {
    perror("Can not allocate memory 106 str");
    freeall();
    return 1;
  }

  int rowsPerChunk = matrix_size / size + (rank < (matrix_size % size));

  int *sendcounts = allocate(size * sizeof(int));
  if (sendcounts == NULL) {
    perror("Can not allocate memory 119 str");
    freeall();
    return 1;
  }

  for (int i = 0; i < size; ++i) {
    sendcounts[i] = matrix_size * (matrix_size / size + (i < (matrix_size % size)));
  }

  int *displs = allocate(size * sizeof(int));

  if (displs == NULL) {
    perror("Can not allocate memory 135 str");
    freeall();
    return 1;
  }

  displs[0] = 0;
  for (int i = 1; i < size; ++i) {
    displs[i] = displs[i - 1] + sendcounts[i - 1];
  }

  double *chunk = allocate(matrix_size * rowsPerChunk * sizeof(double));

  if (chunk == NULL) {
    perror("Can not allocate memory 154 str");
    freeall();
    return 1;
  }

  MPI_Scatterv(A, sendcounts, displs, MPI_DOUBLE, chunk, matrix_size * rowsPerChunk, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  int *recvcounts = allocate(size * sizeof(int));

  if (recvcounts == NULL) {
    perror("Can not allocate memory 171 str");
    freeall();
    return 1
  }

  for (int i = 0; i < size; ++i) {
    recvcounts[i] = matrix_size / size + (i < (matrix_size % size));
  }

  displs[0] = 0;
  for (int i = 1; i < size; ++i) {
    displs[i] = displs[i - 1] + recvcounts[i - 1];
  }

  double *tmp_part = allocate(rowsPerChunk * sizeof(double));

  if (tmp_part == NULL) {
    perror("Can not allocate memory 196 str");
    freeall();
    return 1;
  }

  const double bnorm = VecNorm(b, matrix_size);

  double control = 0;

  double start = 0, end = 0;

  start = MPI_Wtime();

  while (1) {

    MatrVecMUL(tmp_part, chunk, x0, matrix_size, rowsPerChunk);
    MPI_Gatherv(tmp_part, rowsPerChunk, MPI_DOUBLE, tmp, recvcounts, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    if (rank == 0) {
      VecSub(tmp, b, matrix_size);
      NumVecMul(tmp, TAU, matrix_size);
      VecSub(x0, tmp, matrix_size);
    }
    MPI_Bcast(x0, matrix_size, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    MatrVecMUL(tmp, chunk, x0, matrix_size, rowsPerChunk);
    MPI_Gatherv(tmp, rowsPerChunk, MPI_DOUBLE, tmp, recvcounts, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    if (rank == 0) {
      VecSub(tmp, b, matrix_size);
      control = VecNorm(tmp, matrix_size) / bnorm;
    }

    MPI_Bcast(&control, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    if (control < EPS) {
      break;
    }

  }

  end = MPI_Wtime();
  if (rank == 0) {
    for (size_t j = 0; j < matrix_size; ++j) {
      printf("%lf\n", x0[j]);
    }
    printf("\nProcess count: %d\nTime taken: %lf\n\n", size, end - start);
  }

  freeall();

  MPI_Finalize();
  return 0;
}
