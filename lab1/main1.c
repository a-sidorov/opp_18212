#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include <mpi.h>

#define offset(x, y, matrix_size) ((x)*(matrix_size) + (y))
#define TAU 1E-5
#define EPS 1E-9

static void MatrVecMUL(double *Res,
                       const double *A,
                       const double *x,
                       const size_t matrix_size,
                       const size_t rowsPerChunk) {

  for (size_t i = 0; i < rowsPerChunk; ++i) {
    Res[i] = 0.0;
    for (size_t j = 0; j < matrix_size; ++j) {
      Res[i] += A[offset(i, j, matrix_size)] * x[j];
    }
  }
}

static void MatrVecMUL_MPI_part(double *Res_full,
                                const double *chunk,
                                const double *x_part,
                                const size_t matrix_size,
                                const size_t rowsPerChunk) {

  memset(Res_full, 0, matrix_size * sizeof(double));

  for (size_t i = 0; i < rowsPerChunk; i++) {
    for (size_t j = 0; j < matrix_size; j++)
      Res_full[j] += chunk[offset(i, j, matrix_size)] * x_part[i];
  }

  MPI_Allreduce(MPI_IN_PLACE, Res_full, matrix_size, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
}

static void NumVecMul(double *a, const double num, const size_t matrix_size) {
  for (size_t i = 0; i < matrix_size; ++i) {
    a[i] *= num;
  }
}

static void VecSub(double *a, const double *b, const size_t matrix_size) {
  for (size_t j = 0; j < matrix_size; ++j) {
    a[j] = a[j] - b[j];
  }
}

static double VecNorm(const double *x, const size_t matrix_size) {
  double sum = 0.0;
  for (size_t i = 0; i < matrix_size; ++i) {
    sum += x[i] * x[i];
  }
  return sqrt(sum);
}

int main(int argc, char **argv) {

  MPI_Init(&argc, &argv);

  int rank, size;

  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  const int matrix_size = 2500;
  double *A = NULL;
  double *b = NULL;
  double *x0 = NULL;

  if (rank == 0) {
    A = allocate(matrix_size * matrix_size * sizeof(double));
    if (A == NULL) {
      perror("Can not allocate memory 79 str");
      return 1;
    }
    b = allocate(matrix_size * sizeof(double));
    if (b == NULL) {
      perror("Can not allocate memory 84 str");
      freeall();
      return 1;
    }
    x0 = allocate(matrix_size * sizeof(double));
    if (x0 == NULL) {
      perror("Can not allocate memory 90 str");
      freeall();
      return 1;
    }

    for (size_t i = 0; i < matrix_size; ++i) {
      for (size_t j = 0; j < matrix_size; ++j) {
        if (i == j) {
          A[offset(i, j, matrix_size)] = 2.0;
        } else {
          A[offset(i, j, matrix_size)] = 1.0;
        }
      }
    }

    const double bvalue = matrix_size + 1.0;
    for (int i = 0; i < matrix_size; ++i) {
      b[i] = bvalue;
    }
  }

  int rowsPerChunk = matrix_size / size + (rank < (matrix_size % size));

  double *chunk = allocate(matrix_size * rowsPerChunk * sizeof(double));

  if (chunk == NULL) {
    perror("Can not allocate memory 116 str");
    freeall();
    return 1;
  }

  double *b_part = allocate(rowsPerChunk * sizeof(double));

  if (b_part == NULL) {
    perror("Can not allocate memory 128 str");
    freeall();
    return 1;
  }

  double *x0_part = allocate(rowsPerChunk * sizeof(double));

  if (x0_part == NULL) {
    perror("Can not allocate memory 141 str");
    freeall();
    return 1;
  }

  double *tmp = allocate(matrix_size * sizeof(double));

  if (tmp == NULL) {
    perror("Can not allocate memory 155 str");
    freeall();
    return 1;
  }

  double *tmp_part = allocate(rowsPerChunk * sizeof(double));

  if (tmp_part == NULL) {
    perror("Can not allocate memory 171 str");
    freeall();
    return 1;
  }

  int *sendcounts = allocate(size * sizeof(int));

  if (sendcounts == NULL) {
    freeall();
    return 1;
  }

  for (int i = 0; i < size; ++i) {
    sendcounts[i] = matrix_size / size + (i < (matrix_size % size));
  }

  int *displs = allocate(size * sizeof(int));

  if (displs == NULL) {
    perror("Can not allocate memory 210 str");
    freeall();
    return 1;
  }

  displs[0] = 0;
  for (int i = 1; i < size; ++i) {
    displs[i] = displs[i - 1] + sendcounts[i - 1];
  }
  //Send vectors
  MPI_Scatterv(b, sendcounts, displs, MPI_DOUBLE, b_part, rowsPerChunk, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Scatterv(x0, sendcounts, displs, MPI_DOUBLE, x0_part, rowsPerChunk, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  for (int i = 0; i < size; i++) {
    sendcounts[i] *= matrix_size;
  }

  for (int i = 1; i < size; ++i) {
    displs[i] = displs[i - 1] + sendcounts[i - 1];
  }
  //Send Matrix
  MPI_Scatterv(A, sendcounts, displs, MPI_DOUBLE, chunk, rowsPerChunk * matrix_size, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  for (int i = 0; i < size; ++i) {
    sendcounts[i] = matrix_size / size + (i < (matrix_size % size));
  }

  displs[0] = 0;
  for (int i = 1; i < size; ++i) {
    displs[i] = displs[i - 1] + sendcounts[i - 1];
  }

  double bnorm = 0.0;
  if (rank == 0) {
    bnorm = VecNorm(b, matrix_size);
  }

  double control = 0.0;

  double start, end;

  start = MPI_Wtime();

  while (1) {
    MatrVecMUL_MPI_part(tmp, chunk, x0_part, matrix_size, rowsPerChunk);

    MPI_Scatterv(tmp, sendcounts, displs, MPI_DOUBLE, tmp_part, rowsPerChunk, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    VecSub(tmp_part, b_part, rowsPerChunk);
    NumVecMul(tmp_part, TAU, rowsPerChunk);
    VecSub(x0_part, tmp_part, rowsPerChunk);

    MatrVecMUL_MPI_part(tmp, chunk, x0_part, matrix_size, rowsPerChunk);

    if (rank == 0) {
      VecSub(tmp, b, matrix_size);
      control = VecNorm(tmp, matrix_size) / bnorm;
    }

    MPI_Bcast(&control, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    if (control < EPS) {
      break;
    }

  }
  MPI_Gatherv(x0_part, rowsPerChunk, MPI_DOUBLE, x0, sendcounts, displs, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  end = MPI_Wtime();
  if (rank == 0) {
    for (size_t j = 0; j < matrix_size; ++j) {
      printf("%lf\n", x0[j]);
    }

    printf("\nProcess count: %d\nTime taken: %lf\n\n", size, end - start);
  }

  freeall();
  MPI_Finalize();
  return 0;
}
