//
// Created by alexsander on 28.03.19.
//

#include "myalloc.h"

#define MAX_ALLOC_COUNT 65535

static void *allocated_memory[MAX_ALLOC_COUNT] = {NULL};

static size_t ptr_arr_pos = 0;

void *allocate(size_t size) {
  if (ptr_arr_pos == MAX_ALLOC_COUNT) {
    fprintf(stderr, "MAXIMUM ALLOCATIONS LIMIT EXCEEDED");
    return NULL;
  }

  char *new = malloc(size);

  if (new == NULL) {
    fprintf(stderr, "UNABLE TO ALLOCATE MEMORY");
    return NULL;
  }

  memset(new, 0, size);

  allocated_memory[ptr_arr_pos++] = new;

  return new;
}

void freeall(void) {
  for (size_t idx = 0; idx < ptr_arr_pos; idx++) {
    free(allocated_memory[idx]);

    allocated_memory[idx] = NULL;
  }
}

