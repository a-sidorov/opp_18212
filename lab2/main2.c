#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include <omp.h>

#define offset(x, y, matrix_size) ((x)*(matrix_size) + (y))

#define TAU 1E-5
#define STOP_COUNT 320000
#define EPS 1E-9

#define THREADS_COUNT 4

static void MatrVecMUL(double *Res, const double *A, const double *x, const size_t matrix_size) {

#pragma omp for
  for (size_t i = 0; i < matrix_size; ++i) {
    Res[i] = 0;
    for (size_t j = 0; j < matrix_size; ++j) {
      Res[i] += A[offset(i, j, matrix_size)] * x[j];
    }
  }
}

static void NumVecMul(double *a, const double num, const size_t matrix_size) {
#pragma omp for
  for (size_t i = 0; i < matrix_size; ++i) {
    a[i] *= num;
  }
}

static void VecSub(double *a, const double *b, const size_t matrix_size) {
#pragma omp for
  for (size_t j = 0; j < matrix_size; ++j) {
    a[j] -= b[j];
  }
}

static double VecNorm(const double *x, const size_t matrix_size) {
  double sum = 0;
#pragma omp for
  for (size_t i = 0; i < matrix_size; ++i) {
    sum += x[i] * x[i];
  }
  return sqrt(sum);
}

int main() {
  omp_set_num_threads(THREADS_COUNT);

  const size_t matrix_size = 2500;

  double *A = calloc(matrix_size * matrix_size, sizeof(double));
  if (A == NULL) {
    perror("Can not allocate memory");
    return 1;
  }
  double *b = calloc(matrix_size, sizeof(double));

  if (b == NULL) {
    perror("Can not allocate memory");
    free(A);
    return 1;
  }

  for (size_t i = 0; i < matrix_size; ++i) {
    for (size_t j = 0; j < matrix_size; ++j) {
      if (i == j) {
        A[offset(i, j, matrix_size)] = 2.0;
      } else {
        A[offset(i, j, matrix_size)] = 1.0;
      }
    }
  }

  const double bvalue = matrix_size + 1;

  for (int i = 0; i < matrix_size; ++i) {
    b[i] = bvalue;
  }

  double *x0 = calloc(matrix_size, sizeof(double));

  if (x0 == NULL) {
    perror("Can not allocate memory");
    free(A);
    free(b);

    return 1;
  }

  double *tmp = calloc(matrix_size, sizeof(double));

  if (tmp == NULL) {
    perror("Can not allocate memory");
    free(A);
    free(b);

    free(x0);
    return 1;
  }

  double *tmpvec = calloc(matrix_size, sizeof(double));

  if (tmpvec == NULL) {
    perror("Can not allocate memory");
    free(A);
    free(b);
    free(x0);
    free(tmp);
    return 1;
  }
  double MultuplicationTime = 0, SubtractionTime = 0;
  const double bnorm = VecNorm(b, matrix_size);

  double start = omp_get_wtime();
#pragma omp parallel
  {
    for (size_t i = 0; i < STOP_COUNT; ++i) {

      double smt = omp_get_wtime();
      {
        MatrVecMUL(tmpvec, A, x0, matrix_size);
      }
      double emt = omp_get_wtime();
#pragma omp single
      MultuplicationTime += emt - smt;

      smt = omp_get_wtime();
      {
        VecSub(tmpvec, b, matrix_size);
      }
      emt = omp_get_wtime();
#pragma omp single
      SubtractionTime += emt - smt;

      NumVecMul(tmpvec, TAU, matrix_size);
      smt = omp_get_wtime();
      {
        VecSub(x0, tmpvec, matrix_size);
      }
      emt = omp_get_wtime();
#pragma omp single
      SubtractionTime += emt - smt;

      smt = omp_get_wtime();
      {
        MatrVecMUL(tmp, A, x0, matrix_size);
      }
      emt = omp_get_wtime();
#pragma omp single
      MultuplicationTime += emt - smt;

      smt = omp_get_wtime();
      {
        VecSub(tmp, b, matrix_size);
      }
      emt = omp_get_wtime();
#pragma omp single
      SubtractionTime += emt - smt;

      if (VecNorm(tmp, matrix_size) / bnorm < EPS) {
        break;
      }

    }

  };

  double end = omp_get_wtime();

  for (size_t j = 0; j < matrix_size; ++j) {
    // printf("%lf %lf\n", x0[i], u[i]);
    printf("%lf\n", x0[j]);
  }

  printf("\n\nTime taken: %lf\n\n", end - start);
  printf("MulTime: %lf\n", MultuplicationTime);
  printf("SubTime: %lf\n", SubtractionTime);

  free(A);
  free(b);
  free(x0);
  free(tmp);
  free(tmpvec);

  return 0;
}
